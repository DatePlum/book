== Tests

The purpose of testing a product is to guarantee its quality.
A software test provides an objective and independent view
of the implementation status of the corresponding functionality.
Among other things, it allows to evaluate that a given system:

* reacts correctly to all kinds of interactions that can be had with him
* carries out its functions in the manner desired by the customer:
  not only must each function lead to the expected situation,
  but it must do so within the specified time and environment...

It obviously seems impossible to describe even the slightest test case
without having the corresponding specification.
Specifications and tests are therefore, among other things, intrinsically linked
in most development models, such as the <<model_v,"V" model>>.



=== Static analysis

The *static analysis* of the code (_without_ executing it) allows to get
an idea of how he'll behave when he's executed.
Some formal methods of static code analysis exist
(_eg._ Hoare logic); but none of them allow to say for sure
whether the code will produce errors when it is executed.
However, following a number of principles when coding
reduces the _risk of error_ during code execution.

The principles to be applied can sometimes be empirical.
They generally depend on the programming language used
(always initialize a variable before using it,
limit the number of control structures, ...).

The static analysis of the code is nowadays often integrated in
basic development tools for programming languages
which are compilers and IDEs.
A solution like SonarQube is another example of a static analysis tool.

==== Code review

Code review is an empirical method of static analysis that consists of
to have one developer's source code read by another developer.
The purpose is to get an additional perspective on the design and
implementation, and thus increase the quality of the code.



=== Granularity levels

* A *unit test* tests an individual unit of code;
  it is usually a function or an object.
  What is considered is the _smallest testable part_ of a piece of software.
* An *integration test* combines different software modules and tests them
  as a functional group.
* A *system test* is conducted on a _complete_ and _integrated_ system.
  It evaluates how the different functional groups communicate.
* A *validation test* verifies that the system corresponds to the expressed need.

Since in order to test the software at an ever higher level
it is a matter of combining/integrating its different components with each other,
it is legitimate to ask in what order to integrate these components.

There are different approaches to answer this question.

image::tests/dependances.png[caption="Figure 01:", 400px, title="Example of dependencies", alt="Dependencies"]

[bottom-up ]
==== Bottom-up approach

With the *bottom-up* approach, the components are integrated with each other
starting with those with the fewest dependencies.

. First, the set of components without any dependencies
. Then, the set of modules that depend on one or more components of the previous set

And so on, until the highest level component(s) are integrated.

In the previous figure, we would integrate with the "bottom-up" approach
first the components _D_, _E_, _G_ and _H_, then only _F_ and _B_, then _C_, and finally _A_.

===== Advantages

* Since we don't start integration testing of a particular component
  only when the integration tests of _all_ its dependencies have been successful,
  this means that a failed integration of a component indicates that the error is
  in this particular component, not in any of its dependencies. +
  This approach therefore offers a good *error localization*.
* This approach is intuitive and often corresponds to the reality of the development phase.
  (where the dependencies of a component are developed before the component itself),
  the integration tests of a component can be carried out *just after its development*.
* For the same reason, this approach takes advantage of the *reusability* of the components :
  each component is tested with its actual dependencies, which are themselves already developed and integrated.

===== Disadvantages

* The higher-level components, which are often those that directly fill
  the business need, and therefore the only ones that are of real interest for the customer, are tested *last*.
  The quality of the system is therefore only guaranteed at the very end.
* Similarly, this approach is late in validating the software design.
  Indeed, the later is the integration test of a component,
  the later it is to check that it is functioning as intended in the architectural scheme.
* Finally, this approach obviously requires the presence of modules *without dependencies*.

[[top-down]]
==== Top-down approach

With the *top-down* approach, the components are integrated with each other
starting with the highest level ones.

. First the highest level component
. Then the set of components on which the previous one depends directly
. Then the set of direct dependencies of the previous set of components

And so on, until you integrate the component(s) that have no dependencies.

The dependencies of a particular component are, with this approach, replaced by stubs
(see the description of the <<model_evolutive,evolutive (iterative) development model>>).

In the previous figure, we would integrate with the "bottom-up" approach
first the component _A_, then _B_ and _C_, then only _D_, _E_ and _F_, then finally _G_ and _H_.

===== Benefits

* Since the quality of the most important components can be checked (functionally speaking)
  right from the start, this approach allows *rapid prototyping* of the solution.
* The *design is also tested more quickly*:
  the needs of higher-level components are implemented and tested as things progress.
  When reality disagrees with what was originally designed,
  the design can be improved without calling into question too much of what already exists.
  It thus offers an appreciable *flexibility of implementation*.
* Due to the systematic use of stubs, each component is tested "in isolation",
  which provides a good identification of errors.

===== Disadvantages

* The systematic use of stubs can be seen as imposing
  a slightly higher *development and maintenance charge*.
* The risk is to *neglect the lower level components* :
** It is necessary to ensure that these are fully developed and tested, even if they're the last ones to be.
   Indeed it is necessary to keep in mind that even if the tests of the higher level modules are successful,
   there is no guarantee that they will function properly when stubs are replaced by actual dependencies.
** It is sometimes easy to forget to take into account the reusability of components.

==== Other approaches

* The *sandwich* approach attempts to maximize the benefits of the previous two approaches.
  by integrating high and low level components simultaneously and independently.
  On the other hand, the middle level is sometimes neglected.
* The *big bang* approach integrates all components at the same time.
  For obvious reasons of complexity, this approach is difficult to adopt for large projects.



=== Accessibility levels

* Testing software in "*black box*" mode consists in testing its behavior
  without any knowledge of its internal workings.
  A "black box" test only knows _what_ the software has to do,
  and not _how_ it does it. +
  In black box mode, the tester theorically doesn't need to know how to program.
  It can therefore provide a different point of view, but can potentially make useless efforts.
* A test in " *white box* " mode requires to know how the system works in detail,
  for example the data structures involved.
  Such a test pays as much attention to the internal mechanisms of a program than to the results it produces.
  Static analysis obviously always works in white box mode.



=== Functional vs. non-functional tests

A functional test aims to verify the correct implementation of a <<uml_usecases,use case>>.
It is therefore strongly linked to a *need*.

A non-functional test aims to verify the respect of a *constraint*.
This constraint is most of the time of a technical nature.


[[tests_types]]
=== Test types

There are many different types of tests.
Although it is intended to highlight the diversity of possible tests to be carried out,
the following list is not exhaustive.

* *deployment* / *installation* tests check the software in its production environment
* *compatibility tests* check the software behaviour in relation to other existing components
* *smoke tests* / *boot tests* check that the software starts well and performs its minimum functionality
* *soak tests* / *stress tests* / *load tests*
  check that the software continues to meet the need on a large scale,
  or by depriving it of some of its resources...
* *sanity tests* check if the following test should be run
* *non-regression* tests check that existing software functionality is not broken
* *alpha* then *beta* tests, or *pilots* are run on a panel of pre-selected, representative users
* *destructive* tests attempt to destroy all or part of the software's functionalities
* *performance* tests check if the software performance constraints are met
* *security* tests check for security breaches
* *concurrency* tests check the behaviour of the software during a phase of normal usage by multiple users
* *usability* tests, *accessibility* tests: see the quality of <<quality_usability,usability>>
* *localization* tests check that the quality of the software is maintained for all cultures
* *compliance* tests check compliance with a standard, _eg._ by enabling all compiler warnings
* *continuous* tests: see chapter on <<continuous_integration,continuous integration>>



=== Characteristics

Software being the sum of all its components,
if each of these components is independently, comprehensively tested,
and in accordance with its specification, the quality of the whole software should be guaranteed.

In order to get as close as possible to exhaustiveness, the tests of an application should be as detailed as possible.

Ideally, each test should be independent of the others.
By extension, this means that the scope of each test should be limited.
However, within this scope, the test should be as detailed as possible.

==== Test description

===== Initial situation ("_given_")

This is the state in which the system is before applying the behaviour described by the test.

The initial situation of a test corresponds to its *preconditions*: input data and/or a *setup* command sequence.

===== Action ("_when_")

It is a sequence of events (user actions, network message, ...) applied to the system from the initial situation.

This applied behavior often results in a command (or a sequence of commands)
to take the system from the initial situation to a corresponding expected situation.

===== Expected situation ("_then_")

This is the state the system should be in after acting like described from the initial situation.

If the actual (or observed) situation is different from the expected situation, the test _must_ fail.

==== Qualities

===== Isolation

====== Isolation of the tested element

In order to minimize undesirable edge effects during testing,
it is preferable for the element to be tested in insolation;
that is, by replacing its dependencies with objects specifically created for the test.
These specific objects should much simpler, and therefore free of malfunctions, than their "real" equivalents.

This way of testing is similar to the <<top-down,"top-down">> approach.
In practice, however, it is often necessary to find the right compromise with regard to the level of isolation of the tests :

* Theorically, each tested element is perfectly isolated.
  A failure in such a test automatically locates the anomaly in the element in question, since none of its dependencies are used.
  However, due to the multiplicity of specific test objects, the corresponding test code may be difficult to maintain.
* Not testing any component in isolation is guaranteed to save time, since all the produced code elements are used in production.
  However, in case of failed test(s), the analysis is complex, since the anomaly may not only be found in the test object, but also in any of its dependencies, or in the interactions between these different elements.

====== Objects specific to a test context

* [[test_object_dummy]] A _dummy_ is an object without any behavior, which can be used for example to complete a list of call parameters.
  The behavior of the replaced object must not have any influence on the course of the test.
* [[test_object_stub]] A _stub_ is an object whose return is pre-programmed, and is a priori always the same.
  The _stub_ should have the simplest of behaviors.
* [[test_object_fake]] A _fake_ is similar to a _stub_, but a more complex behavior.
  The behavior of a _fake_ is supposed to be similar to the behavior of an object or group of objects used in production.
  However, a _fake_ is indeed an object to be used only in a test context, and should never be used in production!
* [[test_object_spy]] A _spy_ is a _stub_ that provides analysis functionality.
  For example, it allows you to log additional information when using it, or to get some metrics about its use.
* [[test_object_mock]] A _mock_ is an object whose behavior is pre-programmed.
  A _mock_ causes an error if it is used outside of this behavior.

====== Isolating tests from each other

Each test should be able to be performed independently.
In addition, if several tests are executed (_ie._ as a *suite* of tests),
their sequencing should be able to change from one run to the next without altering the result of each test.

Indeed, if a `T` test would depend on the prior execution of another `P` test:

* if both `P` and `T` fail, is the failure of `T` be due solely to the failure of `P`, or to one or more other problems specific to `T`?
* if `P` fails but `T` passes, is the situation normal?
  Shouldn't `T` also fail, since its precondition, `P`, is not fulfilled?
* Does this dependence of `T` on `P` really reflect the specification?

All these questions make analysis and therefore maintenance of the system very complicated.
In the situation described above, it is preferable to consider `P`,
not as an independent test, but as part of the setup of `T`.

====== Fixtures

Most test frameworks offer at least two specific functions, called *fixtures*:

* a `setUp` function to group together commands to set up a context
  (_ie._ an initial situation) common to all tests in a given suite.
  This function is systematically called before each test in the suite.
* a `tearDown` function for grouping context cleanup commands,
  allowing the other tests in the suite to restart from a healthy initial situation.
  This function is systematically called after each test in the suite.

[NOTE.example,caption=""]
====
Here are some operations that are typically done in a `setUp` function:

* Create a specific test database, different from the production database.
  This may help preserve system security (the database contains "false" information),
  to reduce test execution time (the test base is lighter than the production base),
  or simply to make it possible to execute them (if the format of the production data is difficult to reproduce).
* Create a certain number of files (real or virtual).
  If the files are created on the disk, they will obviously have to be deleted by the `tearDown` function.
* Format a disk and install a clean operating system.
  This ensures a healthy test execution context.
* Acquire a device.
  This device must of course be freed by the `tearDown` function.
* Preparation of test data in the form of fakes/stubs/mocks.

====

===== Determinism

The result of a test must be systematic.
As long as no changes are made, a successful test must remain successful, and a failed test must remain failed.

A deterministic test guarantees, among other things, through the use of stubs, mocks and an isolated implementation,
that an observed anomaly is reproducible, and therefore can be corrected.

[NOTE.example,caption=""]
====
Many elements can make a test non-deterministic.
For example:

* the generation of random variables (_random_)
* the _threads_
* system calls: date/time, internationalization, ...

====

===== Completeness

Since a unit test is the validation of a specific behaviour, it is by nature only a partial indicator of quality.
Only the exhaustiveness of the tests therefore makes it possible to guarantee the overall quality of a system.

In practice, it is necessary to test _all_ of the following elements:

* nominal cases, testing the functional capability of the system against its specification,
* error cases, or the way the system reacts to unexpected inputs,
* different types of inputs, including borderline cases.

In practice, it is essential to create one or more new tests as soon as an anomaly (e.g. a regression) is found!

====== Code coverage

Testing the level of code coverage provides a good indicator of the completeness with which the test code validates the source code.

There are different levels of code coverage granularity:

* check that all functions are called;
* check that all instructions are executed;
* for each control structure, check that all runtime paths are used;
* for each boolean expression, check that all operands contribute to its evaluation.
